import { ChakraProvider, CSSReset } from '@chakra-ui/react';
import { addDecorator } from '@storybook/react';

import theme from '../../../global-fragrance-bank/src/theme';

export const parameters = {
    actions: { argTypesRegex: '^on[A-Z].*' },
};

export const Chakra = ({ children }) => (
    <ChakraProvider theme={theme}>
        <CSSReset />
        {children}
    </ChakraProvider>
);

addDecorator((StoryFn) => (
    <Chakra>
        <StoryFn />
    </Chakra>
));
