export const menu = [
    {
        id: '0',
        menuTitle: 'Commercial',
        submenu: [
            {
                id: '0',
                title: 'Suppplier',
                menus: [
                    {
                        id: '0',
                        title: 'Givaudan',
                        url: '/dashboard',
                    },
                    {
                        id: '1',
                        title: 'Firmenich',
                    },
                    {
                        id: '2',
                        title: 'IFF',
                    },
                    {
                        id: '3',
                        title: 'Takasago',
                    },
                    {
                        id: '4',
                        title: 'Robertet',
                    },
                    {
                        id: '5',
                        title: 'Others',
                    },
                ],
            },
            {
                id: '1',
                title: 'Expert',
                menus: [
                    {
                        id: '0',
                        title: 'Star Fragrances',
                    },
                    {
                        id: '1',
                        title: 'Olfactive Gap Analysis',
                    },
                    {
                        id: '2',
                        title: 'Fragrance Fingerprint',
                    },
                ],
            },
            {
                id: '2',
                title: 'Star Fragrances',
                menus: [],
            },
        ],
    },
    {
        id: '1',
        menuTitle: 'Expert',
        submenu: [
            {
                id: '0',
                title: 'Suppplier',
                menus: [
                    {
                        id: '0',
                        title: 'Suppplier',
                        menus: [
                            {
                                id: '0',
                                title: 'Givaudan',
                            },
                            {
                                id: '1',
                                title: 'Firmenich',
                            },
                            {
                                id: '2',
                                title: 'IFF',
                            },
                            {
                                id: '3',
                                title: 'Takasago',
                            },
                            {
                                id: '4',
                                title: 'Robertet',
                            },
                            {
                                id: '5',
                                title: 'Others',
                            },
                        ],
                    },
                ],
            },
            {
                id: '1',
                title: 'Expert',
                menus: [],
            },
            {
                id: '2',
                title: 'Star Fragrances',
                menus: [],
            },
        ],
    },
    {
        id: '2',
        menuTitle: 'Marketing',
        submenu: [
            {
                id: '0',
                title: 'Suppplier',
                menus: [
                    {
                        id: '0',
                        title: 'Suppplier',
                        menus: [
                            {
                                id: '0',
                                title: 'Givaudan',
                            },
                            {
                                id: '1',
                                title: 'Firmenich',
                            },
                            {
                                id: '2',
                                title: 'IFF',
                            },
                            {
                                id: '3',
                                title: 'Takasago',
                            },
                            {
                                id: '4',
                                title: 'Robertet',
                            },
                            {
                                id: '5',
                                title: 'Others',
                            },
                        ],
                    },
                ],
            },
            {
                id: '1',
                title: 'Expert',
                menus: [],
            },
            {
                id: '2',
                title: 'Star Fragrances',
                menus: [],
            },
        ],
    },
    {
        id: '3',
        menuTitle: 'Technical',
        submenu: [
            {
                id: '0',
                title: 'Suppplier',
                menus: [
                    {
                        id: '0',
                        title: 'Suppplier',
                        menus: [
                            {
                                id: '0',
                                title: 'Givaudan',
                            },
                            {
                                id: '1',
                                title: 'Firmenich',
                            },
                            {
                                id: '2',
                                title: 'IFF',
                            },
                            {
                                id: '3',
                                title: 'Takasago',
                            },
                            {
                                id: '4',
                                title: 'Robertet',
                            },
                            {
                                id: '5',
                                title: 'Others',
                            },
                        ],
                    },
                ],
            },
            {
                id: '1',
                title: 'Expert',
                menus: [],
            },
            {
                id: '2',
                title: 'Star Fragrances',
                menus: [],
            },
        ],
    },
    {
        id: '4',
        menuTitle: 'Regulatory',
        submenu: [
            {
                id: '0',
                title: 'Suppplier',
                menus: [
                    {
                        id: '0',
                        title: 'Suppplier',
                        menus: [
                            {
                                id: '0',
                                title: 'Givaudan',
                            },
                            {
                                id: '1',
                                title: 'Firmenich',
                            },
                            {
                                id: '2',
                                title: 'IFF',
                            },
                            {
                                id: '3',
                                title: 'Takasago',
                            },
                            {
                                id: '4',
                                title: 'Robertet',
                            },
                            {
                                id: '5',
                                title: 'Others',
                            },
                        ],
                    },
                ],
            },
            {
                id: '1',
                title: 'Expert',
                menus: [],
            },
            {
                id: '2',
                title: 'Star Fragrances',
                menus: [],
            },
        ],
    },
    {
        id: '5',
        menuTitle: 'Procurement',
        submenu: [
            {
                id: '0',
                title: 'Suppplier',
                menus: [
                    {
                        id: '0',
                        title: 'Suppplier',
                        menus: [
                            {
                                id: '0',
                                title: 'Givaudan',
                            },
                            {
                                id: '1',
                                title: 'Firmenich',
                            },
                            {
                                id: '2',
                                title: 'IFF',
                            },
                            {
                                id: '3',
                                title: 'Takasago',
                            },
                            {
                                id: '4',
                                title: 'Robertet',
                            },
                            {
                                id: '5',
                                title: 'Others',
                            },
                        ],
                    },
                ],
            },
            {
                id: '1',
                title: 'Expert',
                menus: [],
            },
            {
                id: '2',
                title: 'Star Fragrances',
                menus: [],
            },
        ],
    },
];
