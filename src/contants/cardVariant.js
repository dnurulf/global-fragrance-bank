export const cardVariant = {
    info: '#55B2E6',
    danger: '#E35991',
    warning: '#F5C405',
    undefined: '#415A6C',
};
