import { extendTheme } from '@chakra-ui/react';

// foundations - global styling like color, typography, etc
import colors from './foundations/colors';

// components
import Button from './components/Button';

const overrides = {
    // where foundations go
    colors,
    components: {
        Button,
    },
};

export default extendTheme(overrides);
