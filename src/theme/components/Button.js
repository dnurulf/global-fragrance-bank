const Button = {
    variants: {
        menu: {
            padding: '.5rem .625rem',
            margin: '1px 0',
            borderRadius: '.2rem',
            borderBottom: '3px solid #fff',
            transition: 'all .3s',
            h: 'auto',
            _hover: {
                borderBottom: '3px solid #E35991',
            },
        },
    },
};

export default Button;
