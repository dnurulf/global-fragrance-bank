import React from 'react';
import { Flex, Text, Box } from '@chakra-ui/react';
import PropTypes from 'prop-types';

import { cardVariant } from '../../contants';

export function CardSummary(props) {
    const { icon, title, subtitle, variant } = props;

    return (
        <Flex
            alignItems="center"
            bgColor="#fff"
            borderRadius=".2rem"
            padding="1.25rem"
        >
            <Box>{icon}</Box>

            <Box w={5} />

            <Flex flexDirection="column">
                <Text
                    color={cardVariant[variant]}
                    fontSize="30"
                    fontWeight="bold"
                >
                    {title}
                </Text>
                <Text>{subtitle}</Text>
            </Flex>
        </Flex>
    );
}

CardSummary.propTypes = {
    icon: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired,
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string.isRequired,
    variant: PropTypes.oneOf(['info', 'danger', 'warning', 'undefined']),
};

export default CardSummary;
