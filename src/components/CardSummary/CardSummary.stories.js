import React from 'react';
import { SearchIcon } from '@chakra-ui/icons';

import { CardSummary } from './CardSummary.component';

export default {
    component: CardSummary,
    title: 'Components/CardSummary',
};

const Template = (args) => <CardSummary {...args} />;

export const Example = Template.bind({});

Example.args = {
    icon: <SearchIcon />,
    title: '625',
    subtitle: 'this is subtitle of a card',
    variant: 'info',
};
