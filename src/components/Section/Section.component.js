import React from 'react';
import { Box, Text } from '@chakra-ui/react';
import PropTypes from 'prop-types';

export function Section(props) {
    const { title, children } = props;

    return (
        <Box>
            <Text textColor="#E35991" fontWeight="bold">
                {title}
            </Text>

            <Box>{children}</Box>
        </Box>
    );
}

Section.propTypes = {
    title: PropTypes.string.isRequired,
    children: PropTypes.element.isRequired,
};

export default Section;
