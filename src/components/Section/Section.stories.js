import React from 'react';
import { Text } from '@chakra-ui/react';

import { Section } from './Section.component';

export default {
    component: Section,
    title: 'Components/Section',
};

const Template = (args) => (
    <Section {...args}>
        <Text>Hello this is Section</Text>
    </Section>
);

export const Example = Template.bind({});

Example.args = {
    title: 'Supplier',
};
