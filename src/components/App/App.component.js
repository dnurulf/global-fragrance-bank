import React, { useState } from 'react';
import { Box, Flex, Image, Link, Text } from '@chakra-ui/react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { PrivateRoute } from '../../utils';

import { Home, Dashboard, Login } from '../../pages';
import Logo from '../../assets/logo.png';
import { menu } from '../../contants';
import { Dropdown, Section } from '../../components';

export function App() {
    const [isHovered, setIsHovered] = useState();

    const handleDropdownHover = (menuId) => {
        setIsHovered(menuId);
    };

    const handleDropdownMopuseLeave = () => {
        setIsHovered('');
    };

    return (
        <BrowserRouter>
            <Box>
                <Box px={12}>
                    <Flex py={3} alignItems="center">
                        <Image src={Logo} h="100%" w="150px" mr={4} />

                        <Flex>
                            {menu.map((menu) => (
                                <Box key={Math.random()}>
                                    <Dropdown
                                        label={menu.menuTitle}
                                        isHovered={isHovered === menu.id}
                                        onHover={() =>
                                            handleDropdownHover(menu.id)
                                        }
                                        onMouseLeave={handleDropdownMopuseLeave}
                                    >
                                        <Flex flexDirection="row">
                                            {menu.submenu.map((submenu) => (
                                                <Box
                                                    key={Math.random()}
                                                    minW="150px"
                                                >
                                                    <Section
                                                        title={submenu.title}
                                                    >
                                                        <Flex flexDirection="column">
                                                            {submenu.menus.map(
                                                                (menu) => (
                                                                    <Link
                                                                        key={Math.random()}
                                                                        href="/dashboard"
                                                                    >
                                                                        {
                                                                            menu.title
                                                                        }
                                                                    </Link>
                                                                )
                                                            )}
                                                        </Flex>
                                                    </Section>
                                                </Box>
                                            ))}
                                            <Box w={10} />
                                        </Flex>
                                    </Dropdown>
                                </Box>
                            ))}
                        </Flex>
                    </Flex>

                    <Switch>
                        {/* <Route exact path="/?isLogin=true" component={Login} /> */}
                        <Route exact path="/" component={Home} />
                        <Route exact path="/dashboard" component={Dashboard} />
                    </Switch>
                </Box>

                <Flex
                    alignItems="center"
                    justifyContent="center"
                    py="3rem"
                    bgColor="#415A6C"
                >
                    <Text color="#fff">Global Fragrance Bank © 2021</Text>
                </Flex>
            </Box>
        </BrowserRouter>
    );
}

export default App;
