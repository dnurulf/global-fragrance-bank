import React from 'react';
import { Text } from '@chakra-ui/react';

import { Dropdown } from './Dropdown.component';

export default {
    component: Dropdown,
    title: 'Components/Dropdown',
};

const Template = (args) => (
    <Dropdown {...args}>
        <Text>Hello this is dropdown</Text>
    </Dropdown>
);

export const Example = Template.bind({});

Example.args = {
    label: 'Commercial',
    isHovered: false,
};
