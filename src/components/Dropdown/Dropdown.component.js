import React from 'react';
import { Box, Button, Text } from '@chakra-ui/react';
import { ChevronDownIcon, ChevronUpIcon } from '@chakra-ui/icons';
import PropTypes from 'prop-types';

export function Dropdown(props) {
    const { label, children, onHover, onMouseLeave, isHovered = false } = props;

    return (
        <Box
            position="relative"
            onMouseOver={onHover}
            onMouseLeave={onMouseLeave}
        >
            <Button variant="menu">
                <Text mr={3}>{label}</Text>

                {isHovered ? <ChevronUpIcon /> : <ChevronDownIcon />}
            </Button>

            {isHovered && (
                <Box
                    boxShadow="0 0.25rem 2rem rgb(0 0 0 / 8%)"
                    p={10}
                    position="absolute"
                    bgColor="#fff"
                    zIndex="9999999999999999"
                >
                    {children}
                </Box>
            )}
        </Box>
    );
}

Dropdown.propTypes = {
    label: PropTypes.string.isRequired,
    children: PropTypes.element.isRequired,
    isHovered: PropTypes.bool.isRequired,
    onHover: PropTypes.func.isRequired,
    onMouseLeave: PropTypes.func.isRequired,
};

export default Dropdown;
