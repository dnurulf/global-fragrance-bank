import React from 'react';
import {
    Box,
    HStack,
    Text,
    Flex,
    Image,
    FormControl,
    RadioGroup,
    Radio,
    VStack,
    IconButton,
    Table,
    Thead,
    Tr,
    Th,
    Tbody,
    Td,
    Tfoot,
    TableCaption,
    Spacer,
    Select,
} from '@chakra-ui/react';

import { CardSummary, Section } from '../../components';

import Icon1 from '../../assets/bg1.png';
import Icon2 from '../../assets/bg2.png';
import Icon3 from '../../assets/bg3.png';
import Icon4 from '../../assets/bg4.png';

export function Dashboard() {
    return (
        <Box>
            <HStack>
                <Flex
                    w="200px"
                    flexDirection="column"
                    justifyContent="flex-start"
                >
                    <Box
                        borderRight="1px #ddd solid"
                        borderBottom="1px #ddd solid"
                        pb="25px"
                    >
                        <Text
                            fontWeight="700"
                            color="#E35991"
                            textTransform="uppercase"
                        >
                            Filter
                        </Text>
                    </Box>

                    <Flex
                        flexDirection="column"
                        borderRight="1px solid #ddd"
                        maxH="700px"
                        overflow="auto"
                        pt={5}
                    >
                        <Section title="Supplier">
                            <FormControl as="fieldset" my="5">
                                <RadioGroup defaultValue="Itachi">
                                    <VStack spacing="5px" alignItems="stretch">
                                        <Radio value="1">CFF</Radio>
                                        <Radio value="2">Firmenich</Radio>
                                        <Radio value="3">Iff</Radio>
                                        <Radio value="4">Mane</Radio>
                                        <Radio value="4">Robertet</Radio>
                                        <Radio value="4">Sozio</Radio>
                                    </VStack>
                                </RadioGroup>
                            </FormControl>
                        </Section>

                        <Section title="Olfactive Families">
                            <FormControl as="fieldset" my="5">
                                <RadioGroup defaultValue="Itachi">
                                    <VStack spacing="5px" alignItems="stretch">
                                        <Radio value="1">Agrest Marine</Radio>
                                        <Radio value="2">Agrestic</Radio>
                                        <Radio value="3">Almond</Radio>
                                        <Radio value="4">Amber</Radio>
                                        <Radio value="4">Ambery</Radio>
                                        <Radio value="4">Animal</Radio>
                                        <Radio value="4">Apple</Radio>
                                        <Radio value="4">Aquatic</Radio>
                                        <Radio value="4">Aromatic</Radio>
                                        <Radio value="4">Balsamic</Radio>
                                        <Radio value="4">Berry</Radio>
                                        <Radio value="4">Cedarwood</Radio>
                                        <Radio value="4">Cinnamon</Radio>
                                        <Radio value="4">Citronella</Radio>
                                        <Radio value="4">Citrus</Radio>
                                        <Radio value="4">Citrus Fruity</Radio>
                                        <Radio value="4">Coconut</Radio>
                                        <Radio value="4">Cologne</Radio>
                                        <Radio value="4">Cologne</Radio>
                                        <Radio value="4">Cologne</Radio>
                                        <Radio value="4">Cologne</Radio>
                                        <Radio value="4">Cologne</Radio>
                                    </VStack>
                                </RadioGroup>
                            </FormControl>
                        </Section>
                    </Flex>
                </Flex>

                <Box
                    flex="1"
                    p={10}
                    justifyContent="flex-start"
                    alignItems="flex-start"
                >
                    <Flex alignItems="center" mb="3">
                        <Box>
                            <Select placeholder="Select option">
                                <option value="option1">Option 1</option>
                                <option value="option2">Option 2</option>
                                <option value="option3">Option 3</option>
                            </Select>
                        </Box>

                        <Spacer />

                        <Flex alignItems="center">
                            <Box mr="2">
                                <Select placeholder="Select option">
                                    <option value="option1">Option 1</option>
                                    <option value="option2">Option 2</option>
                                    <option value="option3">Option 3</option>
                                </Select>
                            </Box>

                            <Box>
                                <IconButton mr="2" />
                                <IconButton />
                            </Box>
                        </Flex>
                    </Flex>

                    <Box>
                        <Table variant="striped" colorScheme="gray">
                            <Thead bgColor="#f4f6fa" border="2px solid #e6ebf4">
                                <Tr>
                                    <Th p="10px" border="1px solid #eee">
                                        Fragrance Name
                                    </Th>
                                    <Th p="10px" border="1px solid #eee">
                                        Segment
                                    </Th>
                                    <Th p="10px" border="1px solid #eee">
                                        Brand
                                    </Th>
                                    <Th p="10px" border="1px solid #eee">
                                        Fragrance
                                    </Th>
                                    <Th p="10px" border="1px solid #eee">
                                        Olfactive Family
                                    </Th>
                                </Tr>
                            </Thead>
                            <Tbody border="2px solid #e6ebf4">
                                <Tr>
                                    <Td p="10px" border="1px solid #eee">
                                        ADJ 5832 DITO EJ002648/00
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        dish
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        finish
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        givaudan
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        aromatic
                                    </Td>
                                </Tr>
                                <Tr>
                                    <Td p="10px" border="1px solid #eee">
                                        ALADDIN 20 EAJ13303/00
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fabric care
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        vanish
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        givaudan
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fougere
                                    </Td>
                                </Tr>
                                <Tr>
                                    <Td p="10px" border="1px solid #eee">
                                        ALADDIN 20 EAJ13303/00
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fabric care
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        vanish
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        givaudan
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fougere
                                    </Td>
                                </Tr>
                                <Tr>
                                    <Td p="10px" border="1px solid #eee">
                                        ALADDIN 20 EAJ13303/00
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fabric care
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        vanish
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        givaudan
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fougere
                                    </Td>
                                </Tr>
                                <Tr>
                                    <Td p="10px" border="1px solid #eee">
                                        ALADDIN 20 EAJ13303/00
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fabric care
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        vanish
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        givaudan
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fougere
                                    </Td>
                                </Tr>
                                <Tr>
                                    <Td p="10px" border="1px solid #eee">
                                        ALADDIN 20 EAJ13303/00
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fabric care
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        vanish
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        givaudan
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fougere
                                    </Td>
                                </Tr>
                                <Tr>
                                    <Td p="10px" border="1px solid #eee">
                                        ALADDIN 20 EAJ13303/00
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fabric care
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        vanish
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        givaudan
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fougere
                                    </Td>
                                </Tr>
                                <Tr>
                                    <Td p="10px" border="1px solid #eee">
                                        ALADDIN 20 EAJ13303/00
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fabric care
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        vanish
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        givaudan
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fougere
                                    </Td>
                                </Tr>
                                <Tr>
                                    <Td p="10px" border="1px solid #eee">
                                        ALADDIN 20 EAJ13303/00
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fabric care
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        vanish
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        givaudan
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fougere
                                    </Td>
                                </Tr>
                                <Tr>
                                    <Td p="10px" border="1px solid #eee">
                                        ALADDIN 20 EAJ13303/00
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fabric care
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        vanish
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        givaudan
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fougere
                                    </Td>
                                </Tr>
                                <Tr>
                                    <Td p="10px" border="1px solid #eee">
                                        ALADDIN 20 EAJ13303/00
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fabric care
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        vanish
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        givaudan
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fougere
                                    </Td>
                                </Tr>
                                <Tr>
                                    <Td p="10px" border="1px solid #eee">
                                        ALADDIN 20 EAJ13303/00
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fabric care
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        vanish
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        givaudan
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fougere
                                    </Td>
                                </Tr>
                                <Tr>
                                    <Td p="10px" border="1px solid #eee">
                                        ALADDIN 20 EAJ13303/00
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fabric care
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        vanish
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        givaudan
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fougere
                                    </Td>
                                </Tr>
                                <Tr>
                                    <Td p="10px" border="1px solid #eee">
                                        ALADDIN 20 EAJ13303/00
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fabric care
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        vanish
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        givaudan
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fougere
                                    </Td>
                                </Tr>
                                <Tr>
                                    <Td p="10px" border="1px solid #eee">
                                        ALADDIN 20 EAJ13303/00
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fabric care
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        vanish
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        givaudan
                                    </Td>
                                    <Td p="10px" border="1px solid #eee">
                                        fougere
                                    </Td>
                                </Tr>
                            </Tbody>
                        </Table>
                    </Box>
                </Box>

                <Flex
                    flexDirection="column"
                    bgColor="#ECEFF1"
                    p="1.5rem"
                    h="100vh"
                >
                    <CardSummary
                        icon={
                            <Image
                                h="60px"
                                w="60px"
                                borderRadius="50%"
                                src={Icon1}
                            />
                        }
                        title="615"
                        subtitle="Total Fragrance MODS"
                        variant="info"
                    />

                    <Box h="30px" />

                    <CardSummary
                        icon={
                            <Image
                                h="60px"
                                w="60px"
                                borderRadius="50%"
                                src={Icon2}
                            />
                        }
                        title="615"
                        subtitle="Total Health MODS"
                        variant="danger"
                    />

                    <Box h="30px" />

                    <CardSummary
                        icon={
                            <Image
                                h="60px"
                                w="60px"
                                borderRadius="50%"
                                src={Icon3}
                            />
                        }
                        title="615"
                        subtitle="Total Hygiene Mods"
                        variant="warning"
                    />

                    <Box h="30px" />

                    <CardSummary
                        icon={
                            <Image
                                h="60px"
                                w="60px"
                                borderRadius="50%"
                                src={Icon4}
                            />
                        }
                        title="615"
                        subtitle="Total Health MODS"
                        variant="undefined"
                    />
                </Flex>
            </HStack>
        </Box>
    );
}

export default Dashboard;
