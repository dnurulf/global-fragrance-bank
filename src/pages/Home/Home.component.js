import React, { useState } from 'react';
import { Box, Flex, Image, HStack, Text } from '@chakra-ui/react';
import { VectorMap } from '@south-paw/react-vector-maps';

import { CardSummary } from '../../components';
import worldLowRes from '../../contants/world.json';

import Icon1 from '../../assets/bg1.png';
import Icon2 from '../../assets/bg2.png';
import Icon3 from '../../assets/bg3.png';
import Icon4 from '../../assets/bg4.png';

import styles from './Maps.module.scss';

export function Home() {
    const [hovered, setHovered] = useState(' ');

    const layerProps = {
        onMouseEnter: ({ target }) => setHovered(target.attributes.name.value),
        onMouseLeave: () => setHovered(' '),
    };

    const renderListCategory = (color, text) => (
        <Flex flexDirection="row" alignItems="center" mr={4}>
            <Box h="10px" w="10px" bgColor={color} borderRadius="50%" mr={2} />
            <Text>{text}</Text>
        </Flex>
    );

    return (
        <Box>
            <HStack>
                <Box flex="1">
                    <div className={styles.Maps}>
                        <VectorMap {...worldLowRes} layerProps={layerProps} />

                        <Flex mt={3}>
                            {renderListCategory('#579B3B', 'AMEA')}
                            {renderListCategory('#94458F', 'EU')}
                            {renderListCategory('#E76C1F', 'AMEA')}
                            {renderListCategory('#E4491F', 'LATAM')}
                            {renderListCategory('#4599D6', 'NA')}
                        </Flex>
                    </div>
                </Box>

                <Flex
                    flexDirection="column"
                    bgColor="#ECEFF1"
                    p="1.5rem"
                    h="100vh"
                >
                    <CardSummary
                        icon={
                            <Image
                                h="60px"
                                w="60px"
                                borderRadius="50%"
                                src={Icon1}
                            />
                        }
                        title="615"
                        subtitle="Total Fragrance MODS"
                        variant="info"
                    />

                    <Box h="30px" />

                    <CardSummary
                        icon={
                            <Image
                                h="60px"
                                w="60px"
                                borderRadius="50%"
                                src={Icon2}
                            />
                        }
                        title="615"
                        subtitle="Total Health MODS"
                        variant="danger"
                    />

                    <Box h="30px" />

                    <CardSummary
                        icon={
                            <Image
                                h="60px"
                                w="60px"
                                borderRadius="50%"
                                src={Icon3}
                            />
                        }
                        title="615"
                        subtitle="Total Hygiene Mods"
                        variant="warning"
                    />

                    <Box h="30px" />

                    <CardSummary
                        icon={
                            <Image
                                h="60px"
                                w="60px"
                                borderRadius="50%"
                                src={Icon4}
                            />
                        }
                        title="615"
                        subtitle="Total Health MODS"
                        variant="undefined"
                    />
                </Flex>
            </HStack>
        </Box>
    );
}

export default Home;
